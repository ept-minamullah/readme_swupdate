# SW update Using Single Image Streaming

## Scenario
How have to deploy a complete update system but due to memory limitation we can't update in dual bank partition update system. but SW update support single slot update mechanism so can we try it out in Forklifter project.

## Add meta-swupdate 

First of all add the meta-update to build now the next problem is that the meta-swupdate is compatible with U-BOOT by default as in the single copy method we dont need the bootloader as in the double copy method the bootloader decides the which partition should run after successful updation.So in case of single image streaming we dont need the bootloader, that decides the which partition will run, so we have single rootfs everytime it will runs automatically so the first task is to the bootloader configurations in the meta-swupdate.
To disable the configurations of swupdate goto the defconfig file in meta-swupdate and disable the bootloader as shown

```
#CONFIG_UBOOT=y
# CONFIG_BOOTLOADER_NONE is not set
# CONFIG_BOOTLOADER_GRUB is not set
#CONFIG_UBOOT_FWENV="/etc/fw_env.config"
#CONFIG_UBOOT_DEFAULTENV="/etc/u-boot-initial-env"
```

After disabling the bootloader configurations, add the following lines to image file (.bb) 

```
IMAGE_INSTALL_append = "swupdate \
					swupdate-www \
					"
```
### Testing on Board

Build the image and test it on the board 

![Alt text](swupdate_rnning.png)

### Accessing the SWUPDATE server

Swupdate server accessed at <ip_address>:8080

![Alt text](swupdate_server.png)

## Enable the Initaramfs

Now the next task is to enable the initramfs image in the build that create the initial roofs in the ram so while standing in the initramfs we have to the update the rootfs of the system so to enable the initramfs in the system add the following lines to local.conf file.

```
INITRAMFS_IMAGE_BUNDLE = "1"
INITRAMFS_IMAGE = "phytec-initramfs"
IMAGE_FSTYPES_append = " ubifs cpio.gz ext4.gz"
```

Phytec-initramfs contains the same stuff as core-image-minimal have just added following lines to it 

```
IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
IMAGE_FSTYPES_append = " ubifs cpio.gz ext4.gz
```

Now build the image again this time cpio.gz , ubifs , ext4.gz files created in the images folder. Cpio.gz contain the the rootfs that will use in the initramfs.Ubifs is the rootfs will be used in the NAND. and ext4,gz will used in the sw-discription file.Dumb the image file in the sd card. 

## Make tftp server for boot net 

Tftp server in this process is used to initialize the initramfs as there is memory limitaions to bundle initram with kernel when tried to bundle initram with kernel image the size of kernel image goes upto 42MB so thats why the made the tftp server that contains the kernel image , device tree and the cpio.gz archieve.

### Install and configure tftp server on host machine

so to make the tftp server first install the tftp server on host machine by using following command 

```
sudo apt-get install tftpd-hpa
```
Next, files can be accessed from another machine on the same network by simply using the IP address of the host. Specify a folder where the files will reside on the host by replacing the folder path for TFTP_DIRECTORY with the folder where the tftp files placed. For configurations of tftp server use following command 

```
sudo nano /etc/default/tftpd-hpa
```

Then edit this file using following configurations

```
# /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/tftpboot"
TFTP_ADDRESS="0.0.0.0:69"
TFTP_OPTIONS="--secure --create"
```
Replace TFTP_ADDRESS with the ip address of the host.
### Configurations on board

First stop the boot process at Bootloader then using following command to see the ip address 

```
devinfo eth0
```
This will show 

```
Parameters:
  ethaddr: 00:18:31:e1:d0:26 (type: MAC)
  gateway: 192.168.3.10 (type: ipv4)
  ipaddr: 192.168.3.11 (type: ipv4)
  linux.bootargs:  (type: string)
  linux.devname:  (type: string)
  mode: static (type: enum) (values: "dhcp", "static", "disabled")
  netmask: 255.255.255.0 (type: ipv4)
  serverip: 192.168.3.10 (type: ipv4)
```
Change the  serverip according to the tftp server configuratons so to change the server ip use following command 

```
nv.net.server=0.0.0.0
```
Replace with the ip address of tftp server.
Now configure the bootnet script located in `/env/boot`

Edit the bootnet script as follows 

```
#!/bin/sh

path="/mnt/tftp"

global.bootm.image="${path}/zImage"

oftree="${path}/oftree"
if [ -f "${oftree}" ]; then
	global.bootm.oftree="$oftree"
fi

nfsroot="/tftpboot"

ip_route_get -b ${global.net.server} global.linux.bootargs.dyn.ip

initramfs="${path}/initramfs"
if [ -f "${initramfs}" ]; then
	global.bootm.initrd="$initramfs"
else
	global.linux.bootargs.dyn.root="root=/dev/nfs nfsroot=$nfsroot,v3,tcp"
fi
```
To the save the above configurations use 

```
saveenv
```
Now restart the tftp service on host and on board type boot net to boot the board from net.This will initialize the initramfs.

## Perform the swupdate 

After reaching in initramfs now perform the swupdate from the swupdate webserver. simply go to 
```
0.0.0.0:8080

```
Here replace the 0.0.0.0 with ip adress of the board this will open the swupdate server now place the .swu file here the swu is the archieve contains the sw-description and the updated image that will we installed after successfull updation. sw-discription file is 

```
software =
{
	version = "0.1.0";

	description = "rootfs update for phytec board";

	phycore-am335x-4 = { 
		hardware-compatibility: [ "1.0" ];
		images: (
		{
			filename = "forklifter-image-phycore-am335x-4.ubifs"; 
			installed-directly = "true";
			volume = "root";
		}

		);
	};
}
```
The logs of updation shown below 

![Alt text](<Screenshot from 2023-07-04 10-53-08-1.png>)

The rootfs updated successfully.

## Access and modify Environment variables of Bootloader

Till now first stop at bootloader and give command of `boot net` to goto the initramfs which is not feasible as cant access the bootloader using ssh so there is a solution that access the envirinment variables from the rootfs and modify them in that way that when the board restart it goes starts booting from net and perform updation then again change the environment variable in that way when system reboots it boots from NAND.

There is a utility in barebarebox name **bareboxenv** by using this the environment variables can be access and modified so add this utility to the image using 

```
IMAGE_INSTALL_append = " barebox-targettools"
```
This will install the bareboxenv utility in image as shown 

![Alt text](<Screenshot from 2023-07-04 11-06-53-1.png>)

Then find the location of bareboxenv in mtd so to find this use the cat /proc/mtd command that shows

![Alt text](<Screenshot from 2023-07-04 11-11-27-1.png>)

The environment variables lies in mtd6 shown in above figure 

Now create a directory in `tmp` and load the environment variables to it using following command 

```
bareboxenv -l /tmp/* /dev/mtdblock6
```
here * is the directory created in /tmp

This will load the environment variables to it. Now modify the variables according to requirements and save them back to NAND using 

```
bareboxenv -s /tmp/* /dev/mtdblock6
```
Tried to modify the variables to boot from the net after restart place the following things in that directory 

![Alt text](<Screenshot from 2023-07-03 19-51-08-1.png>)

In boot directory there is net script as added while stoping on bootloader 

```
#!/bin/sh
path="/mnt/tftp"
global.bootm.image="${path}/zImage"
oftree="${path}/oftree"
if [ -f "${oftree}" ]; then
global.bootm.oftree="$oftree"
fi
nfsroot="/tftpboot"
ip_route_get -b ${global.net.server} global.linux.bootargs.dyn.ip
initramfs="${path}/initramfs"
if [ -f "${initramfs}" ]; then
global.bootm.initrd="$initramfs"
else
global.linux.bootargs.dyn.root="root=/dev/nfs nfsroot=$nfsroot,v3,tcp"
fi
```
The config file configure the bootchooser to boot the board from net 

```
#!/bin/sh
global.boot.default=net   
global.bootchooser.targets=net
```
nv directory contains the serverip and ipadress as set while staying on bootloader after this save the enviroonment variables to NAND using following command 

```
bareboxenv -s /tmp/* /dev/mtdblock6
```
when the board reboots it starts booting from `net` and goes to the initramfs so perform the updation here as mention above and then simply reset the varaibales to default by simply creating the an emplty directory to `/tmp` and save the enviroments using 

```
bareboxenv -s /tmp/* /dev/mtdblock6
```
This will change the environments to defaults now when board restarts it starts booting from NAND.

So to automate the process simply created a phyton server that contains the variables that placed earliar in the /tmp/*. Made a shellscript named preupdate.sh that bring these variables from pythone server and place as did in above.

```
#!/bin/sh

mkdir /tmp/inam
cd /tmp/inam
wget http://192.168.18.208:8000/config
echo "config file is here"

mkdir boot
echo"making boot directory"
cd boot/	
wget http://192.168.18.208:8000/net
echo "boot net is here"
cd ..

mkdir nv
echo"making nv directory"
cd nv/
wget http://192.168.18.208:8000/allow_color
wget http://192.168.18.208:8000/boot.watchdog_timeout
wget http://192.168.18.208:8000/bootchooser.last_chosen
wget http://192.168.18.208:8000/bootchooser.net.priority
wget http://192.168.18.208:8000/bootchooser.net.remaining_attempts
wget http://192.168.18.208:8000/dev.eth0.ipaddr
wget http://192.168.18.208:8000/dev.eth0.serverip
wget http://192.168.18.208:8000/linux.bootargs.base
wget http://192.168.18.208:8000/linux.bootargs.rootfs
wget http://192.168.18.208:8000/net.server
wget http://192.168.18.208:8000/set

cd ../../..
echo "saving Env. Variables to mtdblock6"
bareboxenv -s -v /tmp/inam /dev/mtdblock6
```

so now when system is in NAND when user want to update the system simply he runs shell script that bring the variables from phytone server and save them to NAND. when board reboots it boot from net and go to the initramfs and perform updation and these is one more shellscript named postupdate.sh. which is 

```
#!/bin/sh

mkdir /tmp/inam
cd ../..
echo "saving Env. Variables to mtdblock6"
bareboxenv -s -v /tmp/inam /dev/mtdblock6
```
After run this script when board reboots it starts booting from the NAND.


